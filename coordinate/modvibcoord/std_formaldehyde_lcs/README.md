
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Formaldehyde
   
 Example Purpose:  
----------------------------------
  - This example showcase the transformation of normal coordinates 
    into localized coordinates by means of the Jacobi sweep algorithm 
    with the use of a geometrical localization measure as convergence criterium.
   
 Additional Notes: 
----------------------------------

