
MCTDH[2,D] and MCTDH[2,V] wave packet propagation on 6D Henon-Heiles surface
==============

Info on MCTDH
--------------

- Properties: Energy, auto-correlation function
- Expectation values: q, p

