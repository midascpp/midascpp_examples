
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Water
   
 Electronic structure:
----------------------------------
  - HF/STO-3G
   
 Electronic structure program:
----------------------------------
  - CFOUR
   
 Example purpose:  
----------------------------------
  - This example showcase the potential energy and dipole surface construction 
    by means of the adaptive density-guided approach (ADGA), which includes up to 2-mode couplings. 
  
 Additional notes: 
----------------------------------
  - Symmetry is exploited in the surface generation.

