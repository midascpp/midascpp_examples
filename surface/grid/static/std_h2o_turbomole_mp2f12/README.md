
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Water
   
 Electronic Structure:
----------------------------------
  - RI-MP2-F12/cc-pVDZ-F12 
   
 Electronic Structure Program:
----------------------------------
  - Turbomole
   
 Example Purpose:  
----------------------------------
  - This example showcase the potential energy surface construction 
    by means of a static grid approach, which includes up to 3-mode couplings. 
  
 Additional Notes: 
----------------------------------

